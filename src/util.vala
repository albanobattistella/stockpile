/* util.vala
 *
 * Copyright 2023 skøldis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
using GLib;

namespace Jellybean {
    public class JellybeanUtil : Object {
        [Description (nick = "Backend object that stores the jellybeans")]
        Jellybean.Jellybeans[] jellybeans_obj = {};
        private Jellybean.Row[]? rows = {};
        private bool[] is_priority = {};

        construct {
            if (settings.get_string ("jellybeans") == "Item 1,;147,;,;50,;10,;2,;0") {
                string retur = _("Item 1") + ",;147,;,;50,;10,;2,;0"; // Localize first item
                settings.set_string ("jellybeans", retur);
                settings.apply ();
            }
            this.parse ();
        }


        [Description (nick = "Fill in internal jellybean object", blurb = "Parse Jellybean formatted string to Jellybean.Jellybeans[] object")]
        private void parse () {
            // string format inside array: "Item 1,; 50,; Items,; 10,; 2"
            string[] argm;
            Jellybean.Jellybeans jb;
            jellybeans_obj = {};
            string[] parse_args = settings.get_string ("jellybeans").split ("~;");
            debug ("Parsing jellybean-formatted string: " + settings.get_string ("jellybeans"));

            for (int i = 0; i < parse_args.length; i++) {
                argm = parse_args[i].split (",;");
                jb = Jellybean.Jellybeans.from_props (
                    desanitize (argm[0]),
                    double.parse (argm[1]),
                    desanitize (argm[2]),
                    double.parse (argm[3]),
                    double.parse (argm[4]),
                    (argm.length < 6) ? 0 : int.parse (argm[5])
                );
                argm = {};
                jellybeans_obj += jb;
            }
        }

        [Description (nick = "Add or edit a jellybean", blurb = "Add or edit a jellybean at index. If index is -1, jellybean is a new jellybean, else it is editing a jellybean")]
        public void add_item (int index = -1, Jellybean.Jellybeans jellybean = new Jellybean.Jellybeans ())
        requires (index > -2) {
            if (index == -1)
                jellybeans_obj += jellybean;
            else
                jellybeans_obj[index] = jellybean;

            jellybean_update ();
        }

        [Description (nick = "Delete a jellybean", blurb = "Deletes a jellybean at int index")]
        public void delete_item (int index = 0)
        requires (index > -1) {
            if ((jellybeans_obj.length - 1) == index)
                jellybeans_obj.move (index + 1, index, 1);
            else {
                for (int j = index; j < jellybeans_obj.length; j++)
                    jellybeans_obj.move (j + 1, j, 1);
            }
            jellybeans_obj.resize (jellybeans_obj.length - 1); // TODO: fix memleak
            jellybean_update ();
        }

        [Description (nick = "Returns Jellybeans[] object", blurb = "returns whole jellybeans object, for t.ex. constructing the rows")]
        public Jellybean.Jellybeans[] jellybeans_object () {
            return jellybeans_obj;
        }

        [Description (nick = "Compiles a jellybean object into a storable string")]
        private string jellybean_compile (Jellybean.Jellybeans jlb) {
            string name = sanitize (jlb.name);
            string unit = sanitize (jlb.unit);
            string num = jlb.number.to_string ();
            string lo = jlb.low.to_string ();
            string qui = jlb.quick.to_string ();
            string icon = ((int) jlb.icon).to_string ();
            return @"$name,;$num,;$unit,;$lo,;$qui,;$icon";
        }

        [Description (nick = "Updates the stored string with the current value of the Jellybeans[] array")]
        public void jellybean_update () {
            string retur = jellybean_compile (jellybeans_obj[0]);

            if (jellybeans_obj.length > 1) {
                for (int i = 1; i < jellybeans_obj.length; i++) {
                    retur += "~;";
                    retur += jellybean_compile (jellybeans_obj[i]);
                }
            }

            settings.set_string ("jellybeans", retur);
            settings.apply ();
            debug ("Updated saved jellybean-format string: " + settings.get_string ("jellybeans"));
        }

        [Description (nick = "Updates list of jellybeans", blurb = "Updates list of jellybeans stored in mv_lb and binds show_func to each list item")]
        public void update_rows (Gtk.ListBox mv_lb) {         // memory handling issues here with over 6 jellybeans
            int i = 0;

            mv_lb.remove_all ();
            for (i = 0; i < rows.length; i++) {
                rows[i] = null;
            }
            rows = {};
            is_priority = {};

            for (i = 0; i < jellybeans_obj.length; i++) {
                rows += new Jellybean.Row.from_jellybean (i, jellybeans_obj[i]);
                is_priority += (jellybeans_obj[i].number <= jellybeans_obj[i].low);
            }

            reload_rows (mv_lb);
        }

        public bool update_row_at_y (Gtk.ListBox mv_lb, int i = -1) {
            if (i == -1) return false; // We should be using the other function for adding items

            // Remove all the rows first
            mv_lb.remove_all ();

            // Destroy this row
            rows[i] = null;

            // Create a new row
            rows[i] = new Jellybean.Row.from_jellybean (i, jellybeans_obj[i]);    // Create the actual row
            is_priority[i] = (jellybeans_obj[i].number <= jellybeans_obj[i].low); // and set its priority

            // Now we can reload the rows. TODO: reload just the one row
            reload_rows (mv_lb);

            return true;
        }

        public void reload_rows (Gtk.ListBox mv_lb /*, bool extras = false, bool is_dim = false */) {
            int j;
            /* if (extras) {
                    if (is_dim) {
                        for (j = 0; j < rows.length; j++) {
                            rows[j].set_dim (true);
                        }
                    } else {
                        for (j = 0; j < rows.length; j++) {
                            rows[j].set_dim (false);
                        }
                    }
            } */
            for (j = 0; j < jellybeans_obj.length; j++) {
                if (is_priority[j]) // && prioritize_ls)
                    mv_lb.prepend (rows[j]);
                else
                    mv_lb.append (rows[j]);
            }
        }

        private string sanitize (string str) {
            return str.replace (";", "&;&").replace (",", "&,&").replace ("~", "&~&");
        }

        private string desanitize (string str) {
            return str.replace ("&;&", ";").replace ("&,&", ",").replace ("&~&", "~");
        }
    }
}
